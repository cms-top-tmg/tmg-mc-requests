## Introduction slides

- [How to make a MC request](TOPMC-contact_mandate.pdf)
- [How to use the TMG and MccM gitlab](TMG-gitlab-intro-slides.pdf)

## MC Request management flowchart

```mermaid




flowchart TB

O[New request] --> RA
O --> SA



subgraph Request[REQUEST]
direction TB
RA[Request::New]
R0{Cards\navailable} --> |NO| RB
R0 --> |YES| R1
R1{Gridpacks\navailable} --> |NO| RC
R1 --> |YES| RD
RB[Request::SettingUpCards] -->|Generating cards \n PR for genproductions| R1
RC[Request::SettingUpGridpacks] -->|gridpack generated| RD[Request::Validation] --> |local validation successful| RE[Request::Done]

RA --> Rp{pLHE} --> |NO| R0
Rp --> |YES| RL[Request::pLHE] -----> RE
end


subgraph Status[SUBMISSION]
direction TB
SA[Status::New] -->|create MccM Gitlab issue| SB[Status::ToMccM] -->|approval of tickets \n submitted to production| SC[Status::Submitted] -->|production done| SD[Status::Done]

end 

RE <--> SA
HP{Highprio\nrequested?} --> |YES| HY(L3s request high prio) --> SC
HP --> |NO| SC

style RA fill:#dc143c,stroke:#000000,color:#000000
style RB fill:#cd5b45,stroke:#000000,color:#000000
style RC fill:#ed9121,stroke:#000000,color:#000000
style RD fill:#eee600,stroke:#000000,color:#000000
style RE fill:#009966,stroke:#000000,color:#000000
style RL fill:#6699cc,stroke:#000000,color:#000000

style SA fill:#dc143c,stroke:#000000,color:#000000
style SB fill:#ed9121,stroke:#000000,color:#000000
style SC fill:#eee600,stroke:#000000,color:#000000
style SD fill:#009966,stroke:#000000,color:#000000

```
